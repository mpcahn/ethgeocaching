var map
var url = "https://api.thegraph.com/subgraphs/name/mpcahn/geocaching"
var data = `{
  geocaches {
    id
    address
    title
    description
    lat
    long   
  }
}`

var denver = {
  lat: 39.732253,
  lng: -104.987295
}

function spliceSlice(str, index, count, add) {
  // We cannot pass negative indexes directly to the 2nd slicing operation.
  if (index < 0) {
    index = str.length + index;
    if (index < 0) {
      index = 0;
    }
  }

  return str.slice(0, index) + (add || "") + str.slice(index + count);
}

// CREATE THE MAP
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: denver
  });

  // Populate lat and long inputs
  $(document).ready(function () {
    google.maps.event.addListener(map, 'click', function (event) {
      let lat = event.latLng.lat()
      lat = lat.toFixed(5)
      let lng = event.latLng.lng()
      lng = lng.toFixed(5)

      document.getElementById("lat").value = lat
      document.getElementById("long").value = lng

      for (var i = 0; i < locations.length; i++) {
        var data = locations[i];
        var marker = new google.maps.Marker({
          position: {
            lat: data.lat,
            lng: data.lng
          },
          map: map
        });

        attachSecretMessage(marker, i);

      }

      function attachSecretMessage(marker, i) {
        var data = locations[i];

        secretMessage = locations[i].content

        var infowindow = new google.maps.InfoWindow({
          content: secretMessage
        });

        marker.addListener('click', function () {
          infowindow.open(marker.get('map'), marker);
        });
      }
    });
  });

  // Create an array of alphabetical characters used to label the markers.


  // Add some markers to the map.
  // Note: The code uses the JavaScript Array.prototype.map() method to
  // create an array of markers based on a given "locations" array.
  // The map() method here has nothing to do with the Google Maps API.
  // var markers = locations.map(function (location, i) {
  //   return new google.maps.Marker({
  //     position: location      
  //   });
  // });


}

var locations = []

function addScript(url, callback) {
  var script = document.createElement('script');
  if (callback) script.onload = callback;
  script.type = 'text/javascript';
  script.src = url;
  document.body.appendChild(script);
}

function loadMapsAPI() {
  addScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyCpuXgt2Kt4RgdVvpYubHuhve73lCQbAeA&callback=initMap");
}

function getLocations(response) {
  geocaches = response.data.geocaches

  for (let i = 0; i < geocaches.length; i++) {
    let lat = spliceSlice(geocaches[i].lat, 2, 0, '.')
    let lng = spliceSlice(geocaches[i].long, 3, 0, '.')
    let title = geocaches[i].title
    let description = geocaches[i].description
    lat = parseFloat(lat)
    lng = -parseFloat(lng)


    let content = '<div id="content"><h1 id="firstHeading" class="firstHeading">' + title + '</h1><div id="bodyContent"><p>' + description + '</p></div></div>'

    obj = {
      lat,
      lng,
      content
    }

    locations.push(obj)

  }
}

fetch(url, {
  method: "POST",
  mode: "cors",
  cache: "no-cache",
  headers: {
    "Content-Type": "application/json",
    "Accept": "application/json"
  },
  body: JSON.stringify({
    query: data
  })
}).then(res => res.json()).then(
  response => {
    getLocations(response)
    console.log(response)
  }
).catch(error => console.error("Error:", error)).then(
  window.onload = loadMapsAPI,
  loadMapsAPI()
)